const util = require('../../utils/util.js')
import { PetPic } from "../../model/pet.js"
import { RemoteDataService } from '../../utils/remoteDataService.js';
import { UserStorage } from "../../utils/storageSevice.js";

Page({
  data: {
    petId: "",
    userId: "",
    petName: "",
    showDeleteBtn: false,
    uploadImgs: [],
    previewImgs: [],
    showLikeBtn: false,
    picLiker: 0,
    picLikeCount: 0,
    uploadData: {}
  },
  onLoad: function (options) {
    let that = this
    if (options && options.petId) {
      let userInfo = UserStorage.getData()
      let showLikeBtn = true;
      if (userInfo.userId == options.userId) {
        showLikeBtn = false
      }
      that.setData({
        petId: options.petId,
        userId: options.userId,
        petName: options.petName,
        showLikeBtn: showLikeBtn,
        picLiker: options.picLiker,
        picLikeCount: options.picLikeCount
      })
      let params = {
        petId: options.petId
      }
      wx.showNavigationBarLoading()
      RemoteDataService.getUserPetPicList(params).then(result => {
        if (result && result.code == "000") {
          let pics = result.pics
          let tmpImgs = []
          let tmpPreImgs = []
          for(let i=0;i<pics.length;i++){
            tmpImgs.push(new PetPic(pics[i].id, pics[i].petid, pics[i].picPath, false))
            tmpPreImgs.push(pics[i].picPath)
          }
          that.setData({
            uploadImgs: tmpImgs,
            previewImgs: tmpPreImgs
          })
        }
        wx.hideNavigationBarLoading()
      }).catch(err => {
        wx.hideNavigationBarLoading()
      })
    }
  },
  likePet: function(){
    let that = this
    let likeVal = that.data.picLiker
    if (that.data.picLiker == 0) {
      likeVal = 1
    } else {
      likeVal = 0
    }
    let params = {
      petId: that.data.petId,
      userId: that.data.userId,
      picLiker: likeVal
    }
    wx.showNavigationBarLoading()
    RemoteDataService.likePetPic(params).then(result => {
      if (result && result.code == "000") {
        let picLikeCount = that.data.picLikeCount
        if (likeVal == 1) {
          picLikeCount++
        } else {
          picLikeCount--
        }
        that.setData({
          picLiker: likeVal,
          picLikeCount: picLikeCount
        })
        if (likeVal == 1) {
          Common.myToast("点赞成功");
        } else {
          Common.myToast("取消点赞成功");
        }
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
  },
  imgPreview: function(e){
    let that = this;
    wx.previewImage({
      current: e.currentTarget.dataset.src,
      urls: that.data.previewImgs
    })
  },
  imgUpload: function(){
    let that = this
    //init
    that.data.uploadData = {
      i: 0,
      success: 0,
      fail: 0
    }
    wx.chooseImage({
      count: 4, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        let tempFilePaths = res.tempFilePaths
        that.data.uploadData.pics = tempFilePaths
        that.uploadImg()
      }
    })
  },
  uploadImg: function(){
    let that = this
    console.log(that.data.uploadData)
    let udata = that.data.uploadData
    let tmpPic = udata.pics[udata.i]
    let params = {
      uploadType: "petShow",
      petId: that.data.petId
    }
    console.log("tmpPic==" + tmpPic)
    wx.showNavigationBarLoading()
    RemoteDataService.uploadPetAvatar(params, tmpPic, 'avatarFile',function(){
      wx.hideNavigationBarLoading()
      console.log(that.data.uploadData)
      that.data.uploadData.i = that.data.uploadData.i + 1
      if (that.data.uploadData.i == that.data.uploadData.pics.length){
        console.log("complete!")
        console.log("success=" + that.data.uploadData.success)
        console.log("fail==" + that.data.uploadData.fail)
        console.log("i==" + that.data.uploadData.i)
      }else{
        that.uploadImg()
      }
    }).then(result => {
      that.data.uploadData.success = that.data.uploadData.success + 1
      if (result && result.code == "000") {
        // tmpPics.push(new PetPic("", that.data.petId, tmpPic, false))
        let petPics = that.data.uploadImgs
        petPics.push(new PetPic(result.petPicId, that.data.petId, tmpPic, false))
        console.log("tmpPic2==" + tmpPic)
        let tmpPics = that.data.previewImgs
        tmpPics.push(tmpPic)
        that.setData({
          uploadImgs: petPics,
          previewImgs: tmpPics
        })
      }
    }).catch(err => {
      that.data.uploadData.fail = that.data.uploadData.fail + 1
    })
  },
  checkboxChange: function(e){
    let that = this
    let val = e.detail.value
    let tmp = that.data.uploadImgs
    let petPic = tmp[e.currentTarget.dataset.aindex]
    if (val && val.length>0){//checked
      petPic.isDelete = true
    }else{
      petPic.isDelete = false
    }
    that.setData({
      uploadImgs: tmp
    });
  },
  openEdit: function(){
    let that = this;
    that.setData({
      showDeleteBtn: !that.data.showDeleteBtn
    });
  },
  deleteImg: function (e) {
    let that = this
    let imgs = that.data.uploadImgs
    let tmpImgs = []
    let petPicIds = []
    for(let i=0;i<imgs.length;i++){
      if(!imgs[i].isDelete){
        tmpImgs.push(imgs[i])
      }else{
        petPicIds.push(imgs[i].petPicId)
      }
    }
    let params = {
      petPicIds: petPicIds.join(",")
    }
    wx.showNavigationBarLoading()
    RemoteDataService.deletePetPic(params).then(result => {
      if (result && result.code == "000") {
        this.setData({
          uploadImgs: tmpImgs,
          showDeleteBtn: !that.data.showDeleteBtn
        })
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
  },
  imageError: function (e) {
    console.log('image3发生error事件，携带值为', e.detail.errMsg)
  }
})